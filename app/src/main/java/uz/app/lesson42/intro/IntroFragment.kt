package uz.app.lesson42.intro

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.recyclerview.widget.LinearLayoutManager
import uz.app.lesson42.R
import uz.app.lesson42.adapter.PersonAdapter
import uz.app.lesson42.alphabet.AlphabetFragment
import uz.app.lesson42.databinding.FragmentIntroBinding
import uz.app.lesson42.model.Common
import uz.app.lesson42.model.Person2

class IntroFragment : Fragment(R.layout.fragment_intro){
    private var personList = ArrayList<Person2>()
    private val personAdapter by lazy {
        PersonAdapter(onItemClickListener = {
fragmentManager!!.beginTransaction().replace(R.id.frame_layout,AlphabetFragment()).commit()
        })
    }
    private lateinit var binding:FragmentIntroBinding
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentIntroBinding.bind(view)
        initList()
        with(binding){
            recyclerPerson.apply {
                layoutManager = LinearLayoutManager(requireContext())
                adapter = personAdapter
            }
        }
        personAdapter.setElement(personList)

    }

    fun initList(){
        personList = Common.getPersonGroup()
        personList = Common.sortList(personList) as ArrayList<Person2>
        personList = Common.addAlphabets(personList)

    }

//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        if (requestCode == Common.RESULT_CODE && resultCode == Activity.RESULT_OK){
//            val result = data!!.getStringExtra("result")
//            val position = Common.findPositionWithName(result!!,personList)
//            binding.recyclerPerson.smoothScrollToPosition(position)
//        }
//        super.onActivityResult(requestCode, resultCode, data)
//    }


}