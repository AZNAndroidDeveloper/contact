package uz.app.lesson42.adapter

import android.app.Person
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.recyclerview.widget.RecyclerView
import com.amulyakhare.textdrawable.TextDrawable
import com.amulyakhare.textdrawable.util.ColorGenerator
import uz.app.lesson42.databinding.GroupLayoutBinding
import uz.app.lesson42.databinding.PersonLayoutBinding
import uz.app.lesson42.model.Common
import uz.app.lesson42.model.Person2

@Suppress("DUPLICATE_LABEL_IN_WHEN")
class PersonAdapter(
    private val onItemClickListener: (Int) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var element = ArrayList<Person2>()

    fun setElement(personList: ArrayList<Person2>) {
        element.apply { clear(); addAll(personList) }
        notifyDataSetChanged()

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return when (viewType) {
            Common.VIEWTYPE_GROUP -> GroupViewHolder(
                GroupLayoutBinding.inflate(
                    LayoutInflater.from(
                        parent.context
                    ), parent, false
                )
            )
            Common.VIEWTYPE_PERSON -> PersonViewHolder(
                PersonLayoutBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )
            else -> GroupViewHolder(
                GroupLayoutBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        }
    }

    override fun getItemCount(): Int = element.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is GroupViewHolder) {
            holder.bind(element[position])
        } else if (holder is PersonViewHolder) {
            holder.text_person_name.text = element[position].name
            holder.text_person_position.text = element[position].position
            val drawable = TextDrawable.builder().buildRound(
                element[position].name!![0].toString(),
                ColorGenerator.MATERIAL.randomColor
            )
            holder.person_avatar.setImageDrawable(drawable)
        }
    }

    inner class GroupViewHolder(val binding: GroupLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val text_group_title = binding.txtGroupTitle
        fun bind(element: Person2) {
            text_group_title.apply {
                text = element.name
                setOnClickListener {
                    onItemClickListener.invoke(Common.RESULT_CODE)
                }
            }
        }
    }

    inner class PersonViewHolder(binding: PersonLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val text_person_name = binding.txtPersonName
        val text_person_position = binding.txtPersonPosition
        val person_avatar = binding.personAvatar
    }

    override fun getItemViewType(position: Int): Int {
        return element[position].viewType
    }


}