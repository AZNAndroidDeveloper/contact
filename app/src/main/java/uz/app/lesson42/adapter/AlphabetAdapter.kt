package uz.app.lesson42.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.recyclerview.widget.RecyclerView
import com.amulyakhare.textdrawable.TextDrawable
import uz.app.lesson42.databinding.AlphabetItemBinding
import uz.app.lesson42.model.Alphabet
import uz.app.lesson42.model.Common

class AlphabetAdapter(
   private  val onItemClickListener: (Alphabet)->Unit
):RecyclerView.Adapter<AlphabetAdapter.AlphabetViewHolder>() {
    private var alphabets :List<String> = Common.getAlphabetList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlphabetViewHolder = AlphabetViewHolder(AlphabetItemBinding.inflate(
        LayoutInflater.from(parent.context),parent,false))

    override fun getItemCount(): Int  = alphabets.size

    override fun onBindViewHolder(holder: AlphabetViewHolder, position: Int) {
        val drawable:TextDrawable
        val availble_position  = Common.alphabet_available.indexOf(alphabets[position])
        if (availble_position != -1){
            // agar kontack bor bolsa yashil boladi
            drawable = TextDrawable.builder().buildRound(alphabets[position],Color.GREEN)
        }else{
            drawable = TextDrawable.builder().buildRound(alphabets[position],Color.GRAY)

        }
        holder.alphabetItem.setImageDrawable(drawable)

        holder.alphabetItem.apply {
            onItemClickListener(Alphabet(alphabets[position],availble_position))

        }
    }
    inner class AlphabetViewHolder(binding:AlphabetItemBinding):RecyclerView.ViewHolder(binding.root){
        val alphabetItem  = binding.alphabetItem
    }


}