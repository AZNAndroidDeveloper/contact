package uz.app.lesson42.model

class Common {
    companion object {
        val VIEWTYPE_GROUP = 0
        val VIEWTYPE_PERSON = 1
        val RESULT_CODE = 100


        var alphabet_available: MutableList<String> = ArrayList()
        // Alfabit boyicha saralab beradi
        fun sortList(list: ArrayList<Person2>): ArrayList<Person2> {
          list.sortWith(Comparator { o1, o2 ->
                o1!!.name!!.compareTo(o2!!.name!!)
            })
            return list
        }
        //  Alfabitni yaratib beradi
        fun addAlphabets(list:ArrayList<Person2>):ArrayList<Person2>{
            var  i = 0
            val customList = ArrayList<Person2>()
            val person = Person2()
            person.name = list[0].name!![0].toString() // birinchi harfni olish
            person.viewType = VIEWTYPE_GROUP
            alphabet_available.add(list[0].name!![0].toString())
            customList.add(person)

            while (i<list.lastIndex){
                val person = Person2()
                val name1 = list[i].name!![0]
                val name2 = list[i+1].name!![0]
                if (name1==name2){
                    list[i].viewType = VIEWTYPE_PERSON
                    customList.add(list[i])
                }
                else{
                    list[i].viewType = VIEWTYPE_PERSON
                    customList.add(list[i])
                    person.name = name2.toString()
                    person.viewType = VIEWTYPE_GROUP
                    alphabet_available.add(name2.toString())
                    customList.add(person)
                }
                i++
        }
            list[i].viewType = VIEWTYPE_PERSON
            customList.add(list[i])
            return customList
    }

        // isim Indexsini aniqlash
        fun findPositionWithName(name:String,list:ArrayList<Person2>):Int{
            for (i in list.indices){
                if (list[i].name == name)
                    return i
            }
            return -1
        }

        // Alfabit generatsiya qiladi
        fun getAlphabetList():ArrayList<String>{
            val result = ArrayList<String>()
            for(i in 65..90){ // ASCII code
                result.add(i.toChar().toString())
            }
            return result
        }
        // Data yaratiladi

        fun getPersonGroup():ArrayList<Person2>{
            val person  = ArrayList<Person2>()
             person.add(Person2("Aziz","Manager",-1))
             person.add(Person2("Shoxrux","Developer",-1))
             person.add(Person2("Davron","TeamLeader",-1))
             person.add(Person2("Aziza","Doctor",-1))
             person.add(Person2("Nozim ","Director",-1))
             person.add(Person2("Botir","Ishchi",-1))
             person.add(Person2("Ravshan","Manager",-1))
             person.add(Person2("Ravshnoy","Manager",-1))
             person.add(Person2("Yaxyo","Manager",-1))
             person.add(Person2("Oybek","Manager",-1))


return person

        }

}
}