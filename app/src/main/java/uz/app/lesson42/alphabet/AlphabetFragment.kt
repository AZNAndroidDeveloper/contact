package uz.app.lesson42.alphabet

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import uz.app.lesson42.R
import uz.app.lesson42.adapter.AlphabetAdapter
import uz.app.lesson42.databinding.FragmentAlphabetBinding
import uz.app.lesson42.intro.IntroFragment


class AlphabetFragment : Fragment(R.layout.fragment_alphabet) {
    private lateinit var binding: FragmentAlphabetBinding
    private val adapter2 by lazy { AlphabetAdapter(onItemClickListener = {
        if (it.position != -1){
//            fragmentManager!!.beginTransaction().replace(R.id.frame_layout,IntroFragment()).commit()
        }
    }) }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentAlphabetBinding.bind(view)

        with(binding){
            recyclerAlphabet.apply {
                layoutManager = GridLayoutManager(requireContext(),4)
                adapter = adapter2
            }
        }

    }

}